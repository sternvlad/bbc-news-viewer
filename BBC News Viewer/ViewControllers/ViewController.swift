//
//  ViewController.swift
//  BBC News Viewer
//
//  Created by Stern Eduard on 13/05/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit
import SideMenu
import SDWebImage

class ViewController: UIViewController {
    fileprivate var newsViewModel: NewsViewModel?
    @IBOutlet weak var loadingView : UIView!
    @IBOutlet weak var collectionView : UICollectionView!

    override func viewDidLoad() {
        super.viewDidLoad()
        newsViewModel = NewsViewModel ()
        self.refresh()
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    @IBAction func refreshButtonPressed (_sender: Any) {
        self.refresh()
    }
    
    func refresh () {
        newsViewModel?.getNews { success in
            if (success) {
                DispatchQueue.main.async { [weak self] in
                    self?.loadingView.isHidden = true
                    self?.collectionView.reloadData()
                }
            }
        }
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
        if segue.identifier == "segueToDetails" {
            let detailedViewController: DetailedViewController = segue.destination as! DetailedViewController
            detailedViewController.urlString = newsViewModel?.selectedItem.link ?? ""
            
        }
    }
}

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return newsViewModel?.news.count ?? 0
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "newscell", for: indexPath)
            as? NewsCollectionViewCell else {
                return UICollectionViewCell()
        }
        cell.imgView.image = nil
        
        guard let item = newsViewModel?.news[indexPath.row] else {
            return UICollectionViewCell()
        }
        
        cell.titleLabel.text = item.title
        cell.titleLabel.adjustsFontSizeToFitWidth = true
        
        cell.dateLabel.text = item.pubdate
        
        // load images
        cell.imgView.sd_setImage(with: URL(string: item.media ?? ""),
                                 placeholderImage: UIImage(named: "placeholder.png"),
                                 options: .refreshCached)
        
        cell.layer.borderColor = UIColor.lightGray.cgColor
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        // space between cells 10 points
        let padding: CGFloat =  10
        let collectionViewSize = collectionView.frame.size.width - padding
        // width of cell = height of cell
        return CGSize(width: collectionViewSize/2, height: collectionViewSize/2)
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let selectedItem = newsViewModel?.news[indexPath.row] else {
            return
        }
        newsViewModel?.selectedItem = selectedItem
        // check if open browser or go to next ViewController
        if (UserDefaults.standard.bool(forKey: "browserKey")){
            guard let url = URL(string: newsViewModel?.selectedItem.link ?? "") else { return }
            UIApplication.shared.open(url)
        }else {
            performSegue(withIdentifier: "segueToDetails", sender: self)
        }
    }
}
