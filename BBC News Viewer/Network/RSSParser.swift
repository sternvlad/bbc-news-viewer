//
//  RSSParser.swift
//  BBC News Viewer
//
//  Created by Stern Eduard on 14/05/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import Foundation

class RSSParser: NSObject, XMLParserDelegate {
    
    var parser = XMLParser()
    var feeds = NSMutableArray()
    var elements = NSMutableDictionary()
    var element = ""
    var ftitle = ""
    var link = ""
    var img =  ""
    var fdescription = ""
    var fdate = ""
    
    // initilise parser
    func initWithData(data :Data) -> AnyObject {
        startParse(data: data)
        return self
    }
    
    func startParse(data :Data) {
        feeds = []
        parser = XMLParser(data: data)
        parser.delegate = self
        parser.shouldProcessNamespaces = false
        parser.shouldReportNamespacePrefixes = false
        parser.shouldResolveExternalEntities = false
        parser.parse()
    }
    
    func allFeeds() -> NSMutableArray {
        return feeds
    }
    
    func parser(_ parser: XMLParser, didStartElement elementName: String, namespaceURI: String?, qualifiedName qName: String?, attributes attributeDict: [String : String]) {
        element = elementName
        if (element == "item") {
            elements =  NSMutableDictionary()
            elements = [:]
            ftitle = ""
            link = ""
            fdescription = ""
            fdate = ""
        } else if (element as NSString).isEqual(to: "media:thumbnail") {
            if let urlString = attributeDict["url"] {
                img = urlString
            }
        }
    }
    
    func parser(_ parser: XMLParser, didEndElement elementName: String, namespaceURI: String?, qualifiedName qName: String?) {
        
        if (elementName == "item") {
            if ftitle != "" {
                elements.setObject(ftitle, forKey: "title" as NSCopying)
            }
            if link != "" {
                elements.setObject(link, forKey: "link" as NSCopying)
            }
            if fdescription != "" {
                elements.setObject(fdescription, forKey: "description" as NSCopying)
            }
            if fdate != "" {
                elements.setObject(fdate, forKey: "pubDate" as NSCopying)
            }
            if img != "" {
                elements.setObject(img, forKey: "img" as NSCopying)
            }
            feeds.add(elements)
        }
    }
    
    func parser(_ parser: XMLParser, foundCharacters string: String) {
        let currentString = string.trimmingCharacters(in: .whitespacesAndNewlines)
        if element == "title" {
            ftitle += currentString
        } else if element == "link" {
            link += currentString
        } else if element == "description" {
            fdescription += currentString
        } else if element == "pubDate" {
            fdate += currentString
        } else if element == "img" {
            img += currentString
        }
    }
}
