//
//  News+CoreDataProperties.swift
//  BBC News Viewer
//
//  Created by Stern Eduard on 14/05/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//
//

import Foundation
import CoreData


extension News {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<News> {
        return NSFetchRequest<News>(entityName: "News")
    }

    @NSManaged public var title: String?
    @NSManaged public var desc: String?
    @NSManaged public var link: String?
    @NSManaged public var pubdate: String?
    @NSManaged public var media: String?

}
