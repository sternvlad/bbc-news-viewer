//
//  NewsViewModel.swift
//  BBC News Viewer
//
//  Created by Stern Eduard on 15/05/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import Foundation
import UIKit
import SDWebImage

class NewsViewModel {
    var news = [News]()
    var selectedItem = News ()
    
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private let context = (UIApplication.shared.delegate as! AppDelegate).persistentContainer.viewContext
    
    func getNews (completionHandler: @escaping (Bool) -> ())  { // send true if values
        // check if is connected to wifi or celular
        if (Reachability.isConnectedToNetwork()){
            //empty image cache if it's connected
            SDImageCache.shared.clearMemory()
            SDImageCache.shared.clearDisk(onCompletion: {})
            let networkLayer = NetworkLayer ()
            
            networkLayer.getNews { [weak self] (result) in
                switch result {
                case .success(let news) :
                    self?.news = news
                    completionHandler (true)
                case .failure(let error):
                    completionHandler (false)
                    fatalError("error: \(error.localizedDescription)")
                }
            }
        }else {
            // If it's not comnnected to internet get items from Core Data
            DispatchQueue.global(qos: .background).async { [weak self] in
                // avoid creating memory leaks
                guard let strongSelf = self else {
                    return
                }
                // Call your background task
                let coreDataHelper = CoreDataHelper(persistentContainer: strongSelf.appDelegate.persistentContainer)
                let operationQueue: OperationQueue = OperationQueue()
                operationQueue.addOperation {
                    if let news = coreDataHelper.fetchFromStorage() {
                        strongSelf.news = news
                        completionHandler (true)
                    }
                }
                
            }
        }
    }
    
}
