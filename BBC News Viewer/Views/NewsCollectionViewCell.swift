//
//  NewsCollectionViewCell.swift
//  BBC News Viewer
//
//  Created by Stern Eduard on 14/05/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit

class NewsCollectionViewCell: UICollectionViewCell {
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var imgView: UIImageView!
}
