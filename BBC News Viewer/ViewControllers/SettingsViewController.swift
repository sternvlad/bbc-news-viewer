//
//  SettingsViewController.swift
//  BBC News Viewer
//
//  Created by Stern Eduard on 14/05/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit
import SideMenu

class SettingsViewController: UIViewController {
    
    @IBOutlet weak var browserSwitch : UISwitch!

    override func viewDidLoad() {
        super.viewDidLoad()
        // set switch if open browser or not
        browserSwitch.isOn = UserDefaults.standard.bool(forKey: "browserKey")
        // Do any additional setup after loading the view.
    }
    
    @IBAction func menuButtonDidClicked(_ sender: Any) {
        sideMenuController?.revealMenu()
    }
    
    @IBAction func toggleSwitch(_ sender: UISwitch) {
        // save in nsuserdefaults browser open on tap on item
        if(browserSwitch.isOn) {
           UserDefaults.standard.set(true, forKey: "browserKey")
        } else {
            UserDefaults.standard.set(false, forKey: "browserKey")
        }
    }
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
