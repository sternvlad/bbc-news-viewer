//
//  CoreDataHelper.swift
//  BBC News Viewer
//
//  Created by Stern Eduard on 14/05/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import Foundation
import UIKit
import CoreData

class CoreDataHelper {
    
    private let persistentContainer: NSPersistentContainer
    
    init(persistentContainer: NSPersistentContainer) {
        self.persistentContainer = persistentContainer
    }
    
    func clearStorage() {
        // delete all the items from core data
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "News")
        let batchDeleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
        do {
            try managedObjectContext.execute(batchDeleteRequest)
        } catch let error as NSError {
            print(error)
        }
    }
    
    func fetchFromStorage() -> [News]? {
        //get items from core data with nsmanagedobjectcontext
        let managedObjectContext = persistentContainer.viewContext
        let fetchRequest = NSFetchRequest<News>(entityName: "News")
        do {
            let news = try managedObjectContext.fetch(fetchRequest)
            return news
        } catch let error {
            print(error)
            return nil
        }
    }
    
    func saveNews (title: String, desc: String, pubDate: String, link: String, media: String) {
        //save item into CoreData
        let managedObjectContext = persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "News", in: managedObjectContext)
        let newEntry = NSManagedObject(entity: entity!, insertInto: managedObjectContext)
        
        newEntry.setValue(title, forKey: "title")
        newEntry.setValue(desc, forKey: "desc")
        newEntry.setValue(pubDate, forKey: "pubdate")
        newEntry.setValue(link, forKey: "link")
        newEntry.setValue(media, forKey: "media")
        
        do {

            try managedObjectContext.save()
            
        } catch {
            
            print("Failed saving")
        }
        
    }
}
