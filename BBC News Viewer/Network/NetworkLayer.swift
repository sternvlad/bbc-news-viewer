//
//  NetworkLayer.swift
//  BBC News Viewer
//
//  Created by Stern Eduard on 14/05/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import Foundation
import Alamofire
import UIKit
import CoreData
import SDWebImage

private var baseURL = "http://feeds.bbci.co.uk/news/rss.xml"

enum Result<Value> {
    case success(Value)
    case failure(Error)
}

class NetworkLayer {
    
    private var appDelegate = UIApplication.shared.delegate as! AppDelegate
    private var xmlParser: XMLParser!
    
    func getNews (completion: ((Result<[News]>) -> Void)?) {
        let managedObjectContext = appDelegate.persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "News", in: managedObjectContext)
        var feeds : [NSMutableDictionary] = []
        var news = [News]()
        AF.request(baseURL).responseJSON { response in
            if let data = response.data,  let _ = String(data: data, encoding: .utf8) {
                // RSSParser instance/object/variable.
                let rssParser: RSSParser = RSSParser().initWithData(data: data) as! RSSParser                
                // put news into array
                feeds = rssParser.feeds as! [NSMutableDictionary]
        
                for item in feeds {
                    // data format got from server
                    let dateFormatterGet = DateFormatter()
                    dateFormatterGet.dateFormat = "E, d MMM yyyy HH:mm:ss Z"

                    let dateFormatterPrint = DateFormatter()
                    dateFormatterPrint.dateFormat = "dd/MM/yyyy"
                    
                    guard let pubDateGet = item ["pubDate"] else {
                        return
                    }

                    guard let date = dateFormatterGet.date(from: pubDateGet as! String ) else {return}
                    
                    let pubdate = dateFormatterPrint.string(from: date)

                    let newEntry = NSManagedObject(entity: entity!, insertInto: managedObjectContext)
                    
                    guard let title = item ["title"] else {return}
                    guard let desc = item ["description"] else {return}
                    guard let link = item ["link"] else {return}
                    guard let img = item ["img"] else {return}

                    newEntry.setValue(title, forKey: "title")
                    newEntry.setValue(desc, forKey: "desc")
                    newEntry.setValue(pubdate, forKey: "pubdate")
                    newEntry.setValue(link, forKey: "link")
                    newEntry.setValue(img, forKey: "media")

                    news.append(newEntry as! News);

                    do {
                        //save every item into CoreData
                        try managedObjectContext.save()
                    } catch {
                        print("Failed saving")
                    }
                }
                completion?(.success(news))
        }else {
            if let error = response.error {
                completion?(.failure(error))
            }else {
                let error = NSError(domain: "", code: 0, userInfo: [NSLocalizedDescriptionKey : "Data was not retrieved from request"]) as Error
                completion?(.failure(error))
            }
        }
    }
}
}
