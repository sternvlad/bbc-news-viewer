//
//  DetailedViewController.swift
//  BBC News Viewer
//
//  Created by Stern Eduard on 14/05/2019.
//  Copyright © 2019 Stern Eduard. All rights reserved.
//

import UIKit
import WebKit

class DetailedViewController: UIViewController, WKNavigationDelegate {
   
    @IBOutlet weak var wkWebView: WKWebView!
    @IBOutlet weak var loadingView: UIView!
    var urlString = ""

    override func viewDidLoad() {
        super.viewDidLoad()
        wkWebView.navigationDelegate = self
        if let url = URL(string: urlString) {
            let request = URLRequest(url: url)
            wkWebView.load(request)
        }
        // Do any additional setup after loading the view.
    }
    
    //MARK:- WKNavigationDelegate
    
    private func webView(webView: WKWebView, didFailProvisionalNavigation navigation: WKNavigation!, withError error: NSError) {
        print(error.localizedDescription)
    }
    func webView(_ webView: WKWebView, didStartProvisionalNavigation navigation: WKNavigation!) {
        print("Strat to load")
        loadingView.isHidden = false
    }
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        print("finish to load")
        loadingView.isHidden = true
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
